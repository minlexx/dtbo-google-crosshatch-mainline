#!/bin/sh

DTS=sdm660-my-stub-overlay

echo "Compiling DTS ${DTS}..."
dtc -O dtb \
	-o ${DTS}.dtb \
	-b 0 \
	-@ \
	${DTS}.dts

echo "Creating dtbo image..."
mkdtboimg create ${DTS}_stub_dtbo.img \
	--page_size=4096 \
	--id=0 \
	--rev=0 \
	${DTS}.dtb
